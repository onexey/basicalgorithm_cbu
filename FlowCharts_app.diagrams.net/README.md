# Flow Chart

Klasördeki drawio uzantılı dosyaları [diagrams](https://app.diagrams.net) sitesinde açabilirsiniz.

Daha fazla bilgi için aşağıdaki adresleri kullanabilirsiniz.

- [Flowchart Symbols](https://www.smartdraw.com/flowchart/flowchart-symbols.htm)
- [Guide to Flowchart Symbols, from Basic to Advanced](https://www.gliffy.com/blog/how-to-flowchart-basic-symbols-part-1-of-3)
