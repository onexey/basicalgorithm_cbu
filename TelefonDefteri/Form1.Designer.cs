﻿namespace TelefonDefteri
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.dsdsds = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lstPhoneList = new System.Windows.Forms.ListBox();
            this.lstMailList = new System.Windows.Forms.ListBox();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.btnPhoneSave = new System.Windows.Forms.Button();
            this.btnPhoneDelete = new System.Windows.Forms.Button();
            this.btnMailKaydet = new System.Windows.Forms.Button();
            this.btnMailDelete = new System.Windows.Forms.Button();
            this.lstAllList = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtPhoneType = new System.Windows.Forms.TextBox();
            this.txtMailType = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtGuid = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPhone);
            this.groupBox1.Controls.Add(this.txtGuid);
            this.groupBox1.Controls.Add(this.txtMailType);
            this.groupBox1.Controls.Add(this.txtPhoneType);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.btnMailDelete);
            this.groupBox1.Controls.Add(this.btnPhoneDelete);
            this.groupBox1.Controls.Add(this.btnMailKaydet);
            this.groupBox1.Controls.Add(this.btnPhoneSave);
            this.groupBox1.Controls.Add(this.txtMail);
            this.groupBox1.Controls.Add(this.lstMailList);
            this.groupBox1.Controls.Add(this.lstPhoneList);
            this.groupBox1.Controls.Add(this.txtCompany);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtLastName);
            this.groupBox1.Controls.Add(this.dsdsds);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 367);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kayıt gösterim Alanı";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "İsim";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(61, 13);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(100, 20);
            this.txtFirstName.TabIndex = 1;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(61, 39);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(100, 20);
            this.txtLastName.TabIndex = 3;
            // 
            // dsdsds
            // 
            this.dsdsds.AutoSize = true;
            this.dsdsds.Location = new System.Drawing.Point(6, 42);
            this.dsdsds.Name = "dsdsds";
            this.dsdsds.Size = new System.Drawing.Size(37, 13);
            this.dsdsds.TabIndex = 2;
            this.dsdsds.Text = "Soyad";
            // 
            // txtCompany
            // 
            this.txtCompany.Location = new System.Drawing.Point(61, 65);
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.Size = new System.Drawing.Size(100, 20);
            this.txtCompany.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Company";
            // 
            // lstPhoneList
            // 
            this.lstPhoneList.FormattingEnabled = true;
            this.lstPhoneList.Location = new System.Drawing.Point(9, 121);
            this.lstPhoneList.Name = "lstPhoneList";
            this.lstPhoneList.Size = new System.Drawing.Size(120, 95);
            this.lstPhoneList.TabIndex = 6;
            // 
            // lstMailList
            // 
            this.lstMailList.FormattingEnabled = true;
            this.lstMailList.Location = new System.Drawing.Point(170, 121);
            this.lstMailList.Name = "lstMailList";
            this.lstMailList.Size = new System.Drawing.Size(120, 95);
            this.lstMailList.TabIndex = 7;
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(170, 251);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(120, 20);
            this.txtMail.TabIndex = 8;
            // 
            // btnPhoneSave
            // 
            this.btnPhoneSave.Location = new System.Drawing.Point(9, 277);
            this.btnPhoneSave.Name = "btnPhoneSave";
            this.btnPhoneSave.Size = new System.Drawing.Size(120, 23);
            this.btnPhoneSave.TabIndex = 9;
            this.btnPhoneSave.Text = "Telefon Kaydet";
            this.btnPhoneSave.UseVisualStyleBackColor = true;
            this.btnPhoneSave.Click += new System.EventHandler(this.btnPhoneSave_Click);
            // 
            // btnPhoneDelete
            // 
            this.btnPhoneDelete.Location = new System.Drawing.Point(9, 306);
            this.btnPhoneDelete.Name = "btnPhoneDelete";
            this.btnPhoneDelete.Size = new System.Drawing.Size(120, 23);
            this.btnPhoneDelete.TabIndex = 10;
            this.btnPhoneDelete.Text = "Telefon Sil";
            this.btnPhoneDelete.UseVisualStyleBackColor = true;
            this.btnPhoneDelete.Click += new System.EventHandler(this.btnPhoneDelete_Click);
            // 
            // btnMailKaydet
            // 
            this.btnMailKaydet.Location = new System.Drawing.Point(170, 277);
            this.btnMailKaydet.Name = "btnMailKaydet";
            this.btnMailKaydet.Size = new System.Drawing.Size(120, 23);
            this.btnMailKaydet.TabIndex = 9;
            this.btnMailKaydet.Text = "Mail Kaydet";
            this.btnMailKaydet.UseVisualStyleBackColor = true;
            this.btnMailKaydet.Click += new System.EventHandler(this.btnMailKaydet_Click);
            // 
            // btnMailDelete
            // 
            this.btnMailDelete.Location = new System.Drawing.Point(170, 306);
            this.btnMailDelete.Name = "btnMailDelete";
            this.btnMailDelete.Size = new System.Drawing.Size(120, 23);
            this.btnMailDelete.TabIndex = 10;
            this.btnMailDelete.Text = "Mail Sil";
            this.btnMailDelete.UseVisualStyleBackColor = true;
            this.btnMailDelete.Click += new System.EventHandler(this.btnMailDelete_Click);
            // 
            // lstAllList
            // 
            this.lstAllList.FormattingEnabled = true;
            this.lstAllList.Location = new System.Drawing.Point(433, 12);
            this.lstAllList.Name = "lstAllList";
            this.lstAllList.Size = new System.Drawing.Size(313, 329);
            this.lstAllList.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(6, 338);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Kaydet";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtPhoneType
            // 
            this.txtPhoneType.Location = new System.Drawing.Point(9, 225);
            this.txtPhoneType.Name = "txtPhoneType";
            this.txtPhoneType.Size = new System.Drawing.Size(120, 20);
            this.txtPhoneType.TabIndex = 12;
            // 
            // txtMailType
            // 
            this.txtMailType.Location = new System.Drawing.Point(170, 225);
            this.txtMailType.Name = "txtMailType";
            this.txtMailType.Size = new System.Drawing.Size(120, 20);
            this.txtMailType.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(352, 133);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 53);
            this.button1.TabIndex = 8;
            this.button1.Text = "<<<<<";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtGuid
            // 
            this.txtGuid.Location = new System.Drawing.Point(179, 19);
            this.txtGuid.Name = "txtGuid";
            this.txtGuid.Size = new System.Drawing.Size(100, 20);
            this.txtGuid.TabIndex = 14;
            this.txtGuid.Visible = false;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(9, 251);
            this.txtPhone.Mask = "000000000";
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(120, 20);
            this.txtPhone.TabIndex = 15;
            this.txtPhone.ValidatingType = typeof(int);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lstAllList);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCompany;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label dsdsds;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnMailDelete;
        private System.Windows.Forms.Button btnPhoneDelete;
        private System.Windows.Forms.Button btnMailKaydet;
        private System.Windows.Forms.Button btnPhoneSave;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.ListBox lstMailList;
        private System.Windows.Forms.ListBox lstPhoneList;
        private System.Windows.Forms.ListBox lstAllList;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtMailType;
        private System.Windows.Forms.TextBox txtPhoneType;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtGuid;
        private System.Windows.Forms.MaskedTextBox txtPhone;
    }
}

