﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelefonDefteri
{
    public class Person
    {
        public Person()
        {

        }

        public Guid ID { get; set; }


        public string Isim { get; set; }
        public string Soyisim { get; set; }
        public string Tamisim
        {
            get
            {
                return string.Concat(Isim, " ", Soyisim);
            }
        }

        public string Company { get; set; }

        public List<Phone> TelefonListesi { get; set; }
        public List<Mail> MailListesi { get; set; }


        public override string ToString()
        {
            return Tamisim + " " + Company;


        }


    }
}
