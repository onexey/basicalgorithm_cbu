﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelefonDefteri
{
    public class Phone
    {
        public string PhoneType { get; set; }
        public long PhoneNumber { get; set; }

        public Phone()
        {

        }

        public override string ToString()
        {
            return PhoneType + "|" + PhoneNumber.ToString();




        }



    }
}
