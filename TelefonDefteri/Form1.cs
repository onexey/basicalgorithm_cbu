﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace TelefonDefteri
{
    public partial class Form1 : Form
    {

        //"ad" "soyad" "tel (list)" "mail (list)" "firma bilgisi"
        //verileri sakla ve geri yükle

        //class --> person
        //class --> tel ve mail
        //person.telList, person.mailList


        List<Person> butunListe = new List<Person>();

        string FilePath = @"C:\Y\telefondefteri.cbu";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(FilePath))
            {
                var jsonText = System.IO.File.ReadAllText(FilePath);
                butunListe = JsonConvert.DeserializeObject<List<Person>>(jsonText);
                EkraniGuncelle();
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Person KayitOgesi = null;
            KayitOgesi = new Person();
            //if (txtGuid.Text == string.Empty)
            //{



            //}
            //else
            //{

            //    foreach (var item in butunListe)
            //    {
            //        if (item.ID.ToString() == txtGuid.Text)
            //        {
            //            KayitOgesi = item;
            //        }
            //    }

            //}

            KayitOgesi.Isim = txtFirstName.Text;
            KayitOgesi.Soyisim = txtLastName.Text;
            KayitOgesi.Company = txtCompany.Text;

            KayitOgesi.TelefonListesi = new List<Phone>();
            KayitOgesi.MailListesi = new List<Mail>();

            foreach (var item in lstPhoneList.Items)
            {
                if (item is Phone phone)
                {
                    KayitOgesi.TelefonListesi.Add(phone);
                }
            }


            foreach (var item in lstMailList.Items)
            {
                if (item is Mail mail)
                {
                    KayitOgesi.MailListesi.Add(mail);
                }
            }
            if (txtGuid.Text != string.Empty)
            {
                butunListe.RemoveAll(f => f.ID.ToString() == txtGuid.Text);
                KayitOgesi.ID = Guid.Parse(txtGuid.Text);
                butunListe.Add(KayitOgesi);
            }
            else
            {
                KayitOgesi.ID = Guid.NewGuid();
                butunListe.Add(KayitOgesi);
            }

            string jsonButunListe = JsonConvert.SerializeObject(butunListe);

            System.IO.File.WriteAllText(FilePath, jsonButunListe);


            EkraniGuncelle();
            EkraniTemizle();






        }

        private void EkraniTemizle()
        {
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtCompany.Text = string.Empty;
            txtMail.Text = string.Empty;
            txtMailType.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtPhoneType.Text = string.Empty;
            lstMailList.Items.Clear();
            lstPhoneList.Items.Clear();
            txtGuid.Text = string.Empty;
        }

        private void EkraniGuncelle()
        {




            lstAllList.Items.Clear();
            butunListe.ForEach(p =>
            {
                lstAllList.Items.Add(p);
            });
            //foreach (var p in butunListe)
            //{
            //    lstAllList.Items.Add(p);
            //}




        }

        private void btnPhoneSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtPhone.Text))
            {
                Phone phone = new Phone();
                phone.PhoneType = txtPhoneType.Text;
                phone.PhoneNumber = long.Parse(txtPhone.Text);

                lstPhoneList.Items.Add(phone);
            }
            else
            {
                MessageBox.Show("Bu nasıl telefon acaba?!");
                txtPhone.Text = string.Empty;
            }


        }

        private void btnPhoneDelete_Click(object sender, EventArgs e)
        {
            if (lstPhoneList.SelectedIndex != -1)
            {
                lstPhoneList.Items.RemoveAt(lstPhoneList.SelectedIndex);
            }
        }

        private void btnMailKaydet_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtMail.Text) && txtMail.Text.Contains("@"))
            {
                Mail mail = new Mail();
                mail.MailType = txtMailType.Text;
                mail.eMailAddress = txtMail.Text;

                lstMailList.Items.Add(mail);
            }
            else
            {
                MessageBox.Show("Bu nasıl mail adresi?!");
                txtMail.Text = string.Empty;
            }


        }

        private void btnMailDelete_Click(object sender, EventArgs e)
        {
            if (lstMailList.SelectedIndex != -1)
            {
                lstMailList.Items.RemoveAt(lstMailList.SelectedIndex);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (lstAllList.SelectedItem != null)
            {
                EkraniTemizle();
                if (lstAllList.SelectedItem is Person p)
                {
                    txtFirstName.Text = p.Isim;
                    txtLastName.Text = p.Soyisim;
                    txtCompany.Text = p.Company;
                    txtGuid.Text = p.ID.ToString();
                    p.TelefonListesi.ForEach(t =>
                    {
                        lstPhoneList.Items.Add(t);

                    });
                    p.MailListesi.ForEach(m =>
                    {
                        lstMailList.Items.Add(m);
                    });



                }

            }
        }
    }
}
