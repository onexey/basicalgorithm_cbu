﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;


namespace BasicAlgorithm_CBU
{
    class Program
    {

        static int magicVal = int.MinValue;

        static void Main(string[] args)
        {
            //ikisayinintoplami();
            //Sigma();
            //Faktoriyel();
            //RakamlarToplami();
            DaireCevresiHesapla();


        }

        private static void DaireCevresiHesapla()
        {
            var yariCap = SayimiKontrolEtveSayiiseDegeriDon("Yarı çapı giriniz : ");
            if (yariCap != magicVal)
            {
                var cevre = 2 * Math.PI * yariCap;  //2 * pi * r
                Console.WriteLine("Çevre : " + cevre.ToString());
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Yalan yanlış bir değer girildi!! Program kapatılacak");
                Console.ReadKey();
                Environment.Exit(0);
            }





        }

        private static void RakamlarToplami()
        {
            //kullanıcıdan input al 123
            // 1 + 2 + 3
            // neticeyi yaz

            int Sayi = SayimiKontrolEtveSayiiseDegeriDon("Rakamlar toplamı için değer girin: ");
            string sayininstringhali = Sayi.ToString();
            var karakterarray = sayininstringhali.ToCharArray();
            var jjjjfjf = karakterarray;

            int toplam = 0;

            for (int i = 0; i < karakterarray.Length; i++)
            {
                toplam += int.Parse(karakterarray[i].ToString());

            }
            Console.WriteLine("Girilen sayının rakamları toplamı : " + toplam.ToString());
            Console.ReadKey();
        }

        private static void Faktoriyel()
        {
            //kullanıcıdan değeri al
            // 1 den başlayarak değere kadar çarp


            int Sayi = SayimiKontrolEtveSayiiseDegeriDon("Faktöriyel hesabı için bir değer girin : ");

            BigInteger netice = 1;

            for (BigInteger i = 1; i <= Sayi; i++)
            {
                netice = BigInteger.Multiply(netice, i);


            }

            Console.WriteLine("Faktöriyel değeri : " + netice.ToString());
            Console.ReadKey();



        }

        private static void Sigma()
        {
            //kullanıcıdan input al
            // 1 den input dahil sayıları topla
            // neticeyi yaz
            int Sayi = 0;
            int toplam = 0;

            Sayi = SayimiKontrolEtveSayiiseDegeriDon("sigma için değer girin : ");


            for (int i = 0; i <= Sayi; i++)
            {
                toplam += i;
            }

            Console.WriteLine("Sigma değeri : " + toplam.ToString());
            Console.ReadKey();


        }

        static int SayimiKontrolEtveSayiiseDegeriDon(string Soru)
        {
            int sayi = 0;
            bool sayimi = false;
            string sayiconsole = "";
            int sayac = 0;
            int totalHakSayisi = 10;




            while (!sayimi)
            {
                Console.WriteLine(Soru);
                sayiconsole = Console.ReadLine();
                //-2147483648


                if (!int.TryParse(sayiconsole, out var _sayi))
                {
                    Console.WriteLine("Değer bir sayı değil!");
                    Console.WriteLine("Press any key to continue!...");
                    Console.ReadKey();
                    sayac++; //sayac = sayac + 1; sayac += 1;
                    if (sayac >= totalHakSayisi)
                    {
                        //Console.WriteLine("Yeter gari program kapatılacak!!!");
                        //Console.WriteLine("Press any key to continue!...");
                        //Console.ReadKey();
                        break;
                        //Environment.Exit(0);
                    }
                    else
                    {
                        Console.WriteLine("Kalan hakkınız : " + (totalHakSayisi - sayac).ToString());

                    }

                    //
                }
                else
                {
                    if (_sayi == magicVal)
                    {
                        sayac++;
                        Console.WriteLine("Kalan hakkınız : " + (totalHakSayisi - sayac).ToString());

                    }
                    else
                    {
                        sayi = _sayi;
                        sayimi = true;
                    }


                }
            }

            if (sayimi)
            {
                return sayi;
            }
            else
            {
                //throw new Exception("Değer bir sayı değil"); 
                return magicVal;
            }




        }


        static void ikisayinintoplami()
        {

            //string sayi1console = "";
            //string sayi2console = "";
            //bool sayimi = false;
            int sayi1 = 0;
            int sayi2 = 0;

            //int sayac = 0;
            //int totalHakSayisi = 10;
            try
            {
                sayi1 = SayimiKontrolEtveSayiiseDegeriDon("İlk Değeri Girin : ");
                if (sayi1 == magicVal)
                {
                    throw new Exception("Beklenen bir değer girilmedi!");
                }

                sayi2 = SayimiKontrolEtveSayiiseDegeriDon("İkinci Değeri Girin : ");
                if (sayi2 == magicVal)
                {
                    throw new Exception("Beklenen bir değer girilmedi!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Environment.Exit(0);
            }

            //while (!sayimi)
            //{
            //    Console.WriteLine("İlk Değeri Girin : ");
            //    sayi1console = Console.ReadLine();
            //    if (!int.TryParse(sayi1console, out var _sayi1))
            //    {
            //        Console.WriteLine("Değer bir sayı değil!");
            //        Console.WriteLine("Press any key to continue!...");
            //        Console.ReadKey();
            //        sayac++; //sayac = sayac + 1; sayac += 1;
            //        if (sayac >= totalHakSayisi)
            //        {
            //            Console.WriteLine("Yeter gari program kapatılacak!!!");
            //            Console.WriteLine("Press any key to continue!...");
            //            Console.ReadKey();
            //            Environment.Exit(0);
            //        }
            //        else
            //        {
            //            Console.WriteLine("Kalan hakkınız : " + (totalHakSayisi - sayac).ToString());

            //        }

            //        //
            //    }
            //    else
            //    {
            //        sayi1 = _sayi1;
            //        sayimi = true;
            //    }
            //}


            //sayimi = false;

            //sayac = 0;

            //while (!sayimi)
            //{
            //    Console.WriteLine("İkinci Değeri Girin : ");
            //    sayi2console = Console.ReadLine();
            //    if (!int.TryParse(sayi2console, out var _sayi2))
            //    {
            //        Console.WriteLine("Değer bir sayı değil!");
            //        Console.WriteLine("Press any key to continue!...");
            //        Console.ReadKey();
            //        sayac += 1;
            //        if (sayac >= totalHakSayisi)
            //        {
            //            Console.WriteLine("Yeter gari program kapatılacak!!!");
            //            Console.WriteLine("Press any key to continue!...");
            //            Console.ReadKey();
            //            Environment.Exit(0);
            //        }
            //        else
            //        {
            //            Console.WriteLine("Kalan hakkınız : " + (totalHakSayisi - sayac).ToString());

            //        }

            //    }
            //    else
            //    {
            //        sayi2 = _sayi2;
            //        sayimi = true;
            //    }



            //}












            //int sayi1 = 0;

            //try
            //{
            //    sayi1 = int.Parse(sayi1console);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //    Console.ReadKey();
            //    Environment.Exit(0);
            //}


            //if (!int.TryParse(sayi1console, out var sayi1))
            //{
            //    Console.WriteLine("Değer bir sayı değil!");
            //    Console.ReadKey();
            //    Environment.Exit(0);
            //}





            //if (!int.TryParse(sayi2console, out var sayi2))
            //{
            //    Console.WriteLine("Değer bir sayı değil!");
            //    Console.ReadKey();
            //    Environment.Exit(0);
            //}




            //int sayi2 = int.Parse(sayi2console);

            int toplam = sayi1 + sayi2;

            Console.WriteLine("İki sayının toplamı : " + toplam.ToString());

            Console.ReadKey();


        }




        //static int ToplamaYap(int parametre1, int parametre2)
        //{
        //    return parametre1 + parametre2;
        //}

        // çok acayip bir metod dünyayı kurtarır.. 
        //static void Donusuolmayanmetod()
        //{
        //    int sayi1 = 1;
        //    int sayi2 = 1;
        //    int toplam;
        //    toplam = sayi1 + sayi2;

        //}



        //static void girizgah()
        //{
        //    // tipi, adı, değeri
        //    //https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/integral-numeric-types
        //    int Sayi1 = 0;
        //    long LongSayi1 = 0;
        //    decimal decimal1 = 0.1m;
        //    float float1 = 0.0001f;
        //    double double1 = 0.1;
        //    string string1 = "Gürcan & Mesut";
        //    bool bool1 = false; //false


        //    string Sayi2YaziSeklinde = "2";

        //    int Toplam = 0;

        //    Toplam = int.Parse(Sayi2YaziSeklinde) + int.Parse(Sayi2YaziSeklinde);
        //    //4

        //    string Sayi3YaziSeklinde = "üç";
        //    string StringYanyanayazar = "";

        //    StringYanyanayazar = Sayi3YaziSeklinde + Sayi3YaziSeklinde;
        //    //üçüç




        //    int[] arrayint = { 1, 2, 3, 4 };

        //    List<int> listint = new List<int>();

        //    listint.Add(1);



        //    if (bool1)
        //    {

        //    }
        //    else
        //    {

        //    }


        //    if (Sayi1 == 1)
        //    {
        //        Console.WriteLine("Değişken değeri = 1");
        //    }
        //    else if (Sayi1 == 2)
        //    {
        //        Console.WriteLine("Değişken değeri = 2");
        //    }
        //    else if (Sayi1 == 3)
        //    {
        //        Console.WriteLine("Değişken değeri = 3");

        //    }
        //    else
        //    {
        //        Console.WriteLine("Değişken değeri 1 veya 2 değil");
        //    }


        //    switch (Sayi1)
        //    {
        //        case 1:
        //            Console.WriteLine("Değişken değeri = 1");
        //            break;
        //        case 2:
        //            Console.WriteLine("Değişken değeri = 2");
        //            break;
        //        case 3:
        //            Console.WriteLine("Değişken değeri = 3");
        //            break;
        //        default:
        //            Console.WriteLine("Değişken değeri 1 veya 2 değil");
        //            break;
        //    }

        //    //Donusuolmayanmetod();
        //    int Sayi2 = 0;

        //    //int toplamDegiskeni = ToplamaYap(Sayi1, Sayi2);


        //    OrnekClass ornekClass = new OrnekClass();
        //    ornekClass.KapiyiAcPublic();

        //    int ForSonDeger = 5;
        //    for (int i = 0; i < ForSonDeger; i++)
        //    {

        //    }

        //    List<string> isimListesi = new List<string>();
        //    isimListesi.Add("Gürcan");
        //    isimListesi.Add("Mesut");

        //    var isimListesiFarkliOrnek = new List<string>();


        //    for (int i = 0; i < isimListesi.Count - 1; i++)
        //    {
        //        break;
        //    }

        //    foreach (var item in isimListesi)
        //    {
        //        break;
        //    }

        //    while (Sayi1 < 5)
        //    {

        //        if (Sayi1 == 3)
        //        {
        //            break;
        //        }

        //        Sayi1++;
        //    }

        //    do
        //    {
        //        if (Sayi1 == 3)
        //        {
        //            break;
        //        }

        //        Sayi1++;
        //    } while (Sayi1 < 5);

        //}


    }


    public class OrnekClass
    {



        private void KapiyiAc()
        {
            Console.WriteLine("Kapı açıldı!");
        }

        public void KapiyiAcPublic()
        {
            KapiyiAc();
        }



    }

}
